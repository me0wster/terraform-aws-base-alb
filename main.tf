# Create ALB
resource "aws_lb" "alb" {
  load_balancer_type               = "application"
  name                             = var.load_balancer_name
  internal                         = var.load_balancer_is_internal
  security_groups                  = var.security_groups
  subnets                          = var.subnets
  idle_timeout                     = var.idle_timeout
  enable_cross_zone_load_balancing = var.enable_cross_zone_load_balancing
  enable_deletion_protection       = var.enable_deletion_protection
  enable_http2                     = var.enable_http2
  ip_address_type                  = var.ip_address_type

  tags = merge(
    var.tags,
    {
      "Name" = var.load_balancer_name
    },
  )

  timeouts {
    create = var.load_balancer_create_timeout
    delete = var.load_balancer_delete_timeout
    update = var.load_balancer_update_timeout
  }
}

# Create Target Group
resource "aws_lb_target_group" "target_group" {
  count       = "${length(var.target_groups)}"
  name        = var.target_groups[count.index]["name"]
  vpc_id      = var.vpc_id
  port        = var.target_groups[count.index]["port"]
  protocol    = var.target_groups[count.index]["protocol"]
  target_type = "instance"

  health_check {
    path = "/"
    port = var.target_groups[count.index]["port"]
  }

  tags = merge(
    var.tags,
    {
      "Name" = var.target_groups[count.index]["name"]
    },
  )
  depends_on = [aws_lb.alb]

  lifecycle {
    create_before_destroy = true
  }
}

local {
  instance_attachments = "${setproduct(var.instance_ids, ${element(aws_lb_target_group.target_group.*.arn)})}"
}

# Attach ec2 instance to target group
resource "aws_lb_target_group_attachment" "instance_attachment" {
  count            = "${length(var.instance_ids)}"
  target_group_arn = "${element(aws_lb_target_group.target_group.*.arn, count.index)}"
  target_id        = var.instance_ids[count.index]
  port             = 7001
}

# Attach listener to ALB so that it can listen to front end (:80) and send to targetgroup
resource "aws_lb_listener" "frontend" {
  count             = "${length(var.http_tcp_listeners)}"
  load_balancer_arn = "${aws_lb.alb.arn}"
  port              = var.http_tcp_listeners[count.index]["port"]
  protocol          = var.http_tcp_listeners[count.index]["protocol"]

  default_action {
    type             = "forward"
    target_group_arn = "${element(aws_lb_target_group.target_group.*.arn, count.index)}"
  }
}
